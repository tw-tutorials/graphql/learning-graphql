const express = require('express');
const app = express();
const expressGraphQL = require('express-graphql');
const { schema } = require('./schema');

app.use(
	'/api',
	expressGraphQL({
		schema: schema,
		graphiql: true
	})
);
app.use;
app.listen(3001, () => console.log('Server running on port 3001...'));
