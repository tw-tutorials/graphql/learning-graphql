const {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLList,
	GraphQLInt,
	GraphQLNonNull
} = require('graphql');
const { authors, books } = require('./data');

const RootQueryType = new GraphQLObjectType({
	name: 'Query',
	description: 'Root Query',
	fields: () => ({
		books: {
			type: new GraphQLList(BookType),
			description: 'List of all books',
			resolve: getAllBooks
		},
		book: {
			type: BookType,
			description: 'A single book',
			args: {
				id: { type: GraphQLInt }
			},
			resolve: (parent, args) => findBookById(args.id)
		},
		authors: {
			type: new GraphQLList(AuthorType),
			description: 'List of all authors',
			resolve: getAllAuthors
		},
		author: {
			type: AuthorType,
			description: 'A single author',
			args: {
				id: { type: GraphQLInt }
			},
			resolve: (parent, args) => findAuthorById(args.id)
		}
	})
});
const RootMutationType = new GraphQLObjectType({
	name: 'Mutation',
	description: 'Root mutation',
	fields: () => ({
		addBook: {
			type: BookType,
			description: 'Add a book',
			args: {
				name: { type: GraphQLNonNull(GraphQLString) },
				authorId: { type: GraphQLNonNull(GraphQLInt) }
			},
			resolve: (parent, args) => addBook(args)
		},
		addAuthor: {
			type: AuthorType,
			description: 'Add an author',
			args: {
				name: { type: GraphQLNonNull(GraphQLString) }
			},
			resolve: (parent, args) => addAuthor(args)
		}
	})
});

const BookType = new GraphQLObjectType({
	name: 'Book',
	description: 'This represents a book written by an author',
	fields: () => ({
		id: { type: GraphQLNonNull(GraphQLInt) },
		name: { type: GraphQLNonNull(GraphQLString) },
		authorId: { type: GraphQLNonNull(GraphQLInt) },
		author: {
			type: AuthorType,
			resolve: (book) => findAuthorById(book.authorId)
		}
	})
});
const AuthorType = new GraphQLObjectType({
	name: 'Author',
	description: 'This represents an author of a book',
	fields: () => ({
		id: { type: GraphQLNonNull(GraphQLInt) },
		name: { type: GraphQLNonNull(GraphQLString) },
		books: {
			type: new GraphQLList(BookType),
			resolve: getBooksByAuthor
		}
	})
});

function getAllBooks() {
	return books;
}
function getAllAuthors() {
	return authors;
}
function findBookById(id) {
	return books.find((b) => b.id === id);
}
function getBooksByAuthor(author) {
	return books.filter((b) => b.authorId === author.id);
}
function findAuthorById(id) {
	return authors.find((a) => a.id === id);
}
function addBook(args) {
	const book = {
		id: books.length + 1,
		name: args.name,
		authorId: args.authorId
	};
	books.push(book);
	return book;
}
function addAuthor(args) {
	const author = {
		id: authors.length + 1,
		name: args.name
	};
	authors.push(authors);
	return author;
}

const schema = new GraphQLSchema({
	query: RootQueryType,
	mutation: RootMutationType
});

module.exports = { schema };
